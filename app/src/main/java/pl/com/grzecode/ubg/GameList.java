package pl.com.grzecode.ubg;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import pl.com.grzecode.ubg.pl.com.grzecode.ubg.network.GetTaskRequest;


public class GameList extends ListActivity implements Handler.Callback {
    private GamesAdapter adapter;
    private List<Game> games;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_list);

        Bundle b = getIntent().getExtras();

        String j = b.getString("json");

        try {
            games = getGames(j);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter = new GamesAdapter(games);
        setListAdapter(adapter);
    }

    private List<Game> getGames(String json) throws JSONException {
        List<Game> result = new ArrayList<Game>();
        JSONArray data = new JSONArray(json);
        for (int i = 0; i < data.length(); i++) {
            JSONObject g = data.getJSONObject(i);
            Game gm = new Game();
            gm.setName(g.getString("name"));
            gm.setDesc(g.getString("desc"));
            gm.setTaskURL(g.getString("url"));
            result.add(gm);
        }
        return result;
    }
    public void startTask(View view) {
       Button b = (Button) view;
       String t =(String) b.getTag();
        try {
            URL u = new URL(t);
            new GetTaskRequest(new Handler(this),u).execute();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean handleMessage(Message message) {
        return false;
    }

    private class GamesAdapter extends ArrayAdapter<Game> {
        public GamesAdapter(List<Game> items) {
            super(GameList.this, R.layout.games_list_item, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.games_list_item, parent, false);
            }
            TextView text = (TextView) convertView.findViewById(R.id.name);
            Button btn = (Button)convertView.findViewById(R.id.button);
            Game item = getItem(position);
            if (item != null) {
                text.setText(item.getName());
                text.setHint(item.getDesc());
                btn.setTag(item.getTaskURL());
            }
            return convertView;
        }
    }
}