package pl.com.grzecode.ubg.pl.com.grzecode.ubg.network;

import android.os.Handler;

import java.net.MalformedURLException;
import java.util.HashMap;

public class GetGamesRequest extends ServerConnectionTask {
    private static final String servletName = "games";
    private HashMap<String,String> parameters = new HashMap<String, String>();
    public GetGamesRequest(double longitude, double latitude,String nick, Handler handler) throws MalformedURLException {
        super(servletName, handler);
        parameters.put("lgt",""+latitude);
        parameters.put("log",""+longitude);
        parameters.put("nick", nick);
        parameters.put("acr", "0");
    }
    @Override
    HashMap getParameters() {
        return parameters;
    }
}
