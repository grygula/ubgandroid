package pl.com.grzecode.ubg.pl.com.grzecode.ubg.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.DataOutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

public abstract class ServerConnectionTask extends AsyncTask<Void, Void, String> {
    private static final String SERVER_URL = "http://192.168.2.8:3000";
    private final URL URL_TO_EXECUTE;
    private HttpURLConnection connection;
    protected Handler handler;
    private String method = "POST";
    public ServerConnectionTask(String servletName,Handler handler) throws MalformedURLException {
        this(handler, new URL(SERVER_URL + "/" + servletName + ".json"),"POST");
    }
    public ServerConnectionTask(Handler handler, URL url, String method) {
        this.handler = handler;
        URL_TO_EXECUTE = url;
        this.method = method;
    }
    abstract HashMap<String,String> getParameters();

    private String parseParameters()
    {
        HashMap<String,String> params = getParameters();
        StringBuilder parameters = new StringBuilder("ts=").append(System.currentTimeMillis());
        for(String k : params.keySet())
        {
            parameters.append("&");
            parameters.append(k).append("=").append(params.get(k));
        }
        return parameters.toString();
    }
    protected String doInBackground(Void... voids) {
        try {
            String urlParameters = parseParameters();
            connection = (HttpURLConnection) URL_TO_EXECUTE.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod(method);
            connection.setFixedLengthStreamingMode(urlParameters.getBytes().length);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            PrintWriter out = new PrintWriter(connection.getOutputStream());
            out.print(urlParameters);
            out.close();
            int statusCode = connection.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                return "Error: Failed getting update notes";
            }
            String response= "";
            Scanner inStream = new Scanner(connection.getInputStream());
            while(inStream.hasNextLine()){
                response+=(inStream.nextLine());
            }
            return response;
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
    @Override
    protected void onPostExecute(String result) {
        Message message = new Message();
        Bundle data = new Bundle();
        data.putString("json",result);
        message.setData(data);
        handler.sendMessage(message);
    }
}