package pl.com.grzecode.ubg;

import android.app.Activity;
import android.content.Context;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class DataSupplier {
    public static final String FILE_NAME = "data.txt";

    public static boolean hasNameDefined(Activity activity) {
        try {
            String name = getName(activity.openFileInput(FILE_NAME));
            return !(name == null || name.equals(""));
        } catch (FileNotFoundException e) {
            try {
                saveName("",activity);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            return false;
        }
    }

    public static String getName(Activity activity)  {
        try{
            return getName(activity.openFileInput(FILE_NAME));
        }catch (Exception e)
        {

        }
        return "Not found";
    }

    public static String getName(FileInputStream fis) {
        Scanner scanner = null;
        scanner = new Scanner(fis);
        if (scanner.hasNext()) {
            return scanner.nextLine();
        } else {
            return null;
        }
    }

    public static void saveName(String name, Activity activity) throws FileNotFoundException {
        saveName(name, activity.openFileOutput(FILE_NAME, Context.MODE_PRIVATE));
    }

    public static void saveName(String name, FileOutputStream fos) {
        try {
            fos.write(name.getBytes());
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
            }
        }
    }
}
