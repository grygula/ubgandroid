package pl.com.grzecode.ubg;

public final class Game {
    private String name, desc, taskURL;

    public static Game getInstance(Game item) {
        Game copy = new Game();
        copy.name = item.name;
        copy.taskURL = item.taskURL;
        copy.desc = item.desc;
        return copy;
    }

    public String getName() {
        return name;
    }

    public void setName(String i) {
        name = i;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String i) {
        desc = i;
    }

    public String getTaskURL() {
        return taskURL;
    }

    public void setTaskURL(String i) {
        taskURL = i;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Game other = (Game) obj;
        return this.name.equals(other.name);
    }
}
