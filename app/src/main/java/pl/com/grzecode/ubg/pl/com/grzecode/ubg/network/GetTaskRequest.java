package pl.com.grzecode.ubg.pl.com.grzecode.ubg.network;

import android.os.Handler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class GetTaskRequest extends ServerConnectionTask {
    private static final String servletName = "games";
    public GetTaskRequest(Handler handler,URL url) {
        super(handler, url,"GET");
    }
    @Override
    HashMap getParameters() {
        return new HashMap<String, String>();
    }
}
