package pl.com.grzecode.ubg;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import pl.com.grzecode.ubg.pl.com.grzecode.ubg.network.GetGamesRequest;


public class Welcome extends Activity implements Handler.Callback {
    private static EditText nameBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        nameBox = (EditText) findViewById(R.id.nameText);
        if (DataSupplier.hasNameDefined(this)) {
            String n = DataSupplier.getName(this);
            nameBox.setText(n);
        }
    }

    public void letsTheGameBegin(View view) {
        try {
            String nick = nameBox.getText().toString();
            DataSupplier.saveName(nick, this);
            double[] pos = getCurrentPosition();
            try {
                new GetGamesRequest(pos[0],pos[1],nick,new Handler(this)).execute();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
        }
    }

    private double[] getCurrentPosition() {
        double[] result = new double[2];
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        LocationManager gpsManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String providerName = gpsManager.getBestProvider(criteria, true);
        Location l = gpsManager.getLastKnownLocation(providerName);
        if (l != null) {
            result[0] = l.getLatitude();
            result[1] = l.getLongitude();
        } else {
            result[0] = 7;
            result[1] = 2;
        }
        return result;
    }

    @Override
    public boolean handleMessage(Message message) {
        Intent a = new Intent(this, GameList.class);
        a.putExtras(message.getData());
        startActivity(a);
        return true;
    }
}
